const urlGeneral = "http://localhost:3000";
const urlApi = "http://localhost:3000/api/cp_comunidades"; //BASE DE LA URL
let numeroPagina = 0; //EL NUMERO DE LA PAGINA QUE QUIERES CARGAR LA PRIMERA ES LA PAG 0
let maxPags = 0; //CREA VARIABLE DEL NÚMERO MÁXIMO DE PÁGINAS POSIBLES, DEFINIDO EN LA FUNCTION OBTENERDATOS
let numeroAMostrar = 20; // VARIABLE QUE INDICA CUANTAS LINIAS A MOSTRAR EN EL LISTADO PARA DESPUÉS PODER CALCULAR LAS PÁGINA MÁXIMA
let ampliarLista = "?_p=" + numeroPagina + "&_size=" + numeroAMostrar; //FUSION DE TODO PARA CREAR LA URL
let numeroFila=0; //CREA VARIABLE QUE CONTABILIZARA EL NUMERO DE FILA ACTUAL EN EL QUE ESTA


window.onload = () => {
    document.getElementById("formulario").style.display = "none";
    //COUNT DEL TOTAL DE LOS PAISES
    // get(urlPais + "/count")
    //     .then(function (response) {
    //         obtenDatosPaises(JSON.parse(response));
    //         cargarPaises();
    //         console.log(paises);
    //     })
    //     .catch(function (error) {
    //         console.error(error);
    //     });


    //COUNT DEL TOTAL DE TODOS LAS COMUNIDADES AUTONOMAS
    get(urlApi + "/count")
        .then(function (response) {
            obtenDatos(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });

    //CREAR PRIMERA TABLA

    const querie = {
        "query": "SELECT com.cpcoa_id, com.cpcoa_nombre, pais.short_name FROM cp_comunidades com join cp_pais pais on pais.country_id=com.cpcoa_pais limit ?,?",
        "params": [numeroFila, numeroFila+numeroAMostrar]
    }

    numeroFila = numeroFila+numeroAMostrar;

    let json = JSON.stringify(querie);

    getDinamica(urlGeneral + "/dynamic", json)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });




    // get(url)
    // .then((response) => {
    //     console.log(response);
    // })
    // .catch((error) => {
    //     console.log("Error");
    // });

    // let datos = {
    //     "cpcoa_nombre":"Españita de mi vida",
    //     "cpcoa_pais": 209
    // }

    // let json = JSON.stringify(datos);

    // update(url+"1", json)
    // .then((response) => {
    //     console.log(response);
    // })
    // .catch((error) => {
    //     console.log("Error");
    // });

    // insert(urlApi, json)
    //     .then((response) => {
    //         console.log(response);
    //     })
    //     .catch((error) => {
    //         console.log("Error");
    // });

    // purge(url+"20")
    //     .then((response) => {
    //         console.log(response);
    //     })
    //     .catch((error) => {
    //         console.log("Error");
    // });


}

function presentaLista(respuesta) {
    let salida = document.getElementById("salida")
    salida.innerHTML = "";

    //CAMBIAD EL CREAR LOS DATOS SEGÚN VUESTRA TABLA!!!
    respuesta.forEach(row => {

        let tr = document.createElement("tr");
        let td = document.createElement("td");
        td.appendChild(document.createTextNode(row.cpcoa_id));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.cpcoa_nombre));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.short_name));
        tr.appendChild(td);

        td = document.createElement("td");
        let button = document.createElement("button");
        button.setAttribute("class", "btn btn-success");
        button.setAttribute("onclick", "formularioModificar(" + row.cpcoa_id + ")");
        let i = document.createElement("i");
        i.setAttribute("class", "fa fa-pencil-square-o");
        button.appendChild(i);
        td.appendChild(button);
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger ml-2");
        i = document.createElement("i");
        i.setAttribute("class", "fa fa-trash-o");
        button.appendChild(i);
        td.appendChild(button);
        tr.appendChild(td);

        salida.appendChild(tr);

    })
}

function mostrarFormulario(tipoForm) {
    //Oculta el div tabla y borra los datos de la tabla
    document.getElementById("div_tabla").style.display = "none";
    document.getElementById("div_tabla").style.position = "absolute";
    document.getElementById("salida").innerHTML = "";

    //Muestra el formulario y oculta el boton de modificar o insertar
    document.getElementById("formulario").style.display = "block";

    if (tipoForm === "insert") {
        document.getElementById("mod_com").style.display = "none";
    } else {
        document.getElementById("ins_com").style.display = "none";
    }

}

function formularioModificar(id) {
    mostrarFormulario("modify");
    obtenerDatosFormularioPorId(id);
}

function obtenerDatosFormularioPorId(id) {
    get(urlApi + "/" + id)
        .then(function (response) {
            let dataJson = JSON.parse(response);
            rellenarFormulario(dataJson[0]);
        })
        .catch(function (error) {
            console.error(error);
        });
}

function rellenarFormulario(data){
    document.getElementById("id_comunidad").value = data.cpcoa_id;
    document.getElementById("nombre_comunidad").value = data.cpcoa_nombre;
    document.getElementById("prov_pais").value = data.cpcoa_pais;
}

function obtenDatos(respuesta) {
    var registros = respuesta[0].no_of_rows;
    //Máximo de páginas
    maxPags = Math.ceil(registros / numeroAMostrar);
}

function nextPagFunct() {
    // if (numeroPagina < 0) {
    //     numeroPagina = 0;
    // } else if (numeroPagina < maxPags - 1) {
    //     numeroPagina = numeroPagina + 1;
    // }

    numeroFila = numeroFila+numeroAMostrar;

    const querie = {
        "query": "SELECT com.cpcoa_id, com.cpcoa_nombre, pais.short_name FROM cp_comunidades com join cp_pais pais on pais.country_id=com.cpcoa_pais limit ?,?",
        "params": [numeroFila, numeroFila+numeroAMostrar]
    }

    let json = JSON.stringify(querie);

    getDinamica(urlGeneral + "/dynamic", json)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
};


function antPagFunct() {

    if (numeroPagina > 0) {
        numeroPagina = numeroPagina - 1;
    }

    numeroFila = numeroFila-numeroAMostrar*2;

    const querie = {
        "query": "SELECT com.cpcoa_id, com.cpcoa_nombre, pais.short_name FROM cp_comunidades com join cp_pais pais on pais.country_id=com.cpcoa_pais limit ?,?",
        "params": [numeroFila, numeroFila+numeroAMostrar]
    }

    let json = JSON.stringify(querie);

    getDinamica(urlGeneral + "/dynamic", json)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
};



