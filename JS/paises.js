
let tipotabla = "cp_pais" //LA TABLA QUE QUEREMOS CARGAR
let numeroPagina = 0; //EL NUMERO DE LA PAGINA QUE QUIERES CARGAR LA PRIMERA ES LA PAG 0
let ampliarLista = "?_p=" + numeroPagina + "&_size=20"; //FUSION DE TODO PARA CREAR LA URL
const urlApi = "http://localhost:3000/api/"; //BASE DE LA URL

let maxPags = 0; //CREA VARIABLE DEL NÚMERO MÁXIMO DE PÁGINAS POSIBLES, DEFINIDO EN LA FUNCTION OBTENERDATOS
let numeroAMostrar = 20; // VARIABLE QUE INDICA CUANTAS LINIAS A MOSTRAR EN EL LISTADO PARA DESPUÉS PODER CALCULAR LAS PÁGINA MÁXIMA

window.onload = () => {
    document.getElementById("formulario").style.display = "none";

    //COUNT DEL TOTAL DE TODOS LOS PAISES
    get(urlApi + tipotabla + "/count")
        .then(function (response) {
            obtenDatos(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });


    //CREAR PRIMERA TABLA
    get(urlApi + tipotabla + ampliarLista)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
}


function presentaLista(respuesta) {
    let tbody = document.getElementById("tbodyPaises")
    tbody.innerHTML = "";

    //CAMBIAD EL CREAR LOS DATOS SEGÚN VUESTRA TABLA!!!
    respuesta.forEach(row => {

        tr = document.createElement("tr");
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.country_id));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.iso2));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.short_name));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.spanish_name));
        tr.appendChild(td);

        td = document.createElement("td");
        let button = document.createElement("button");
        button.setAttribute("class", "btn btn-success");
        button.setAttribute("onclick", "modificar(" + row.country_id + ")");
        let i = document.createElement("i");
        i.setAttribute("class", "fa fa-pencil-square-o");
        button.appendChild(i);
        td.appendChild(button);
        button = document.createElement("button");
        button.setAttribute("class", "btn btn-danger ml-2");
        button.setAttribute("onclick", "purgeData(" + row.country_id + ")");
        i = document.createElement("i");
        i.setAttribute("class", "fa fa-trash-o");
        button.appendChild(i);
        td.appendChild(button);
        tr.appendChild(td);

        tbody.appendChild(tr);

    });

}

//----------------------------------------
var req; // global variable to hold request object

function obtenDatos(respuesta) {
    var registros = respuesta[0].no_of_rows;
    //console.log(registros);

    //Máximo de páginas
    maxPags = Math.ceil(registros / numeroAMostrar);
    paginacion();
}


//ACTUALIZA LA PAGINACION AL CAMBIAR DE UNA LISTA A OTRA
function paginacion() {
    let divPaginacion = document.getElementById("paginacion");
    divPaginacion.innerHTML = "Página " + (numeroPagina + 1) + " de " + maxPags;
}

//FUNCION PARA IR AL LISTADO SIGUIENTE
function nextPagFunct() {
    //console.log("Funcino siguiente pagina");
    //console.log(numeroPaginaNext);
    if (numeroPagina < 0) {
        numeroPagina = 0;
    } else if (numeroPagina < maxPags - 1) {
        numeroPagina = numeroPagina + 1;
    }

    let ampliarLista = "?_p=" + numeroPagina + "&_size=20"; //FUSION DE TODO PARA CREAR LA URL
    //console.log(numeroPagina);

    get(urlApi + tipotabla + ampliarLista)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
    paginacion();
};


//FUNCION PARA IR A LA LISTA ANTERIOR
function antPagFunct() {
    //console.log("Funcino siguiente pagina");
    //console.log(numeroPaginaNext);

    if (numeroPagina > 0) {
        numeroPagina = numeroPagina - 1;
    }

    let ampliarLista = "?_p=" + numeroPagina + "&_size=20"; //FUSION DE TODO PARA CREAR LA URL
    //console.log(numeroPagina);

    get(urlApi + tipotabla + ampliarLista)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
    paginacion();
};



function mostrarFormulario(tipoForm) {
    //Oculta el div tabla y borra los datos de la tabla
    document.getElementById("div_tabla").style.display = "none";
    document.getElementById("div_tabla").style.position = "absolute";
    document.getElementById("tbodyPaises").innerHTML = "";

    //Muestra el formulario y oculta el boton de modificar o insertar
    document.getElementById("formulario").style.display = "block";

    if (tipoForm === "insert") {
        document.getElementById("mod_com").style.display = "none";
    } else {
        document.getElementById("ins_com").style.display = "none";
    }


}

function modificar(id) {
    mostrarFormulario("modificar");
    obtenerDatosFormularioPorId(id);
}

function obtenerDatosFormularioPorId(id) {
    get(urlApi + tipotabla + "/" + id)
        .then(function (response) {
            let dataJson = JSON.parse(response);
            rellenarFormulario(dataJson[0]);
        })
        .catch(function (error) {
            console.error(error);
        });
}

function rellenarFormulario(data) {
    document.getElementById("name_pais").value = data.short_name;
    document.getElementById("prov_pais").value = data.spanish_name;
    document.getElementById("iso2").value = data.iso2;
}


//FUNCION DE DELETE
function purgeData(id)
{
    purge(urlApi+ tipotabla + "/" +id)
        .then((response) => {
            console.log(response);
            location.reload();
        })
        .catch((error) => {
            console.log("Error");
    });

}


//FUNCION INSERT
function insertData() 
{

let val1 = document.getElementById("name_pais").value;
let val2 = document.getElementById("prov_pais").value;
let val3 = document.getElementById("iso2").value;
let val4 = document.getElementById("id_pais").value;

    get(urlApi+tipotabla)
    .then((response) => {
        console.log(response);
        //location.reload();
    })
    .catch((error) => {
        console.log("Error");
    });

    let datos = {
        "short_name": val1,
        "spanish_name": val2,
        "iso2": val3,
        "country_id": val4,
        "numcode": 0
    }

    let json = JSON.stringify(datos);
    
    insert(urlApi+tipotabla, json)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.log("Error");
    });


}